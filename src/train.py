import argparse

from PostgresHandler import BargainistaPostgresHandler
from MongoHandler import BargainistaMongoHandler
from ModelTrainer import BargainistaModelTrainer

from credentials import postgres_user, postgres_pass, postgres_db, \
    postgres_host, postgres_port, mongo_user, mongo_pass, mongo_auth_db, \
    mongo_host, mongo_port


def parse_arguments() -> argparse.Namespace:
    """
    Parses the arguments feeded to the program.

    Returns:
    arguments   -- Namespace with the arguments feeded to the program
    """
    parser = argparse.ArgumentParser(
        description="Model training of the properties at Bargainista."
    )
    parser.add_argument(
        "--city", default="valencia", type=str,
        help="Name of the city where flats are to be retrieved."
    )
    parser.add_argument(
        "--output_path", default="output", type=str,
        help="Path to the directory where the debug output should be stored."
    )
    parser.add_argument(
        "--workers", default="1", type=int,
        help="Number of threads used in the model training."
    )
    parser.add_argument(
        "--threshold", default="0.8", type=float,
        help="Price quartile from which the luxury properties are ignored."
    )

    return parser.parse_args()


if __name__ == "__main__":
    # Parse the arguments
    args = parse_arguments()

    # Initialize the connections to the databases
    database = BargainistaPostgresHandler(
        postgres_host, postgres_port, postgres_user, postgres_pass,
        postgres_db, args.city
    )
    mongo_store = BargainistaMongoHandler(
        mongo_host, mongo_port, mongo_user, mongo_pass, mongo_auth_db,
        args.city
    )

    # Create the model trainer and load it with the data
    model_trainer = BargainistaModelTrainer(
        database,
        mongo_store,
        args.threshold,
        args.workers)

    # Train the model
    model_trainer.train_model()
    # Create some debug output of the model
    model_trainer.debug_model(args.output_path)
    # Save the model into the MongoDB database
    model_trainer.save_model()

    # Close all the open connections
    del(database)
    del(mongo_store)
    del(model_trainer)
