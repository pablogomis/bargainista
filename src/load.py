import argparse
from MongoHandler import BargainistaMongoHandler
from PostgresHandler import BargainistaPostgresHandler
from Transformator import clean_flat_data, flat_health_check

from credentials import mongo_user, mongo_pass, mongo_auth_db, mongo_host, \
    mongo_port, postgres_user, postgres_pass, postgres_db, postgres_host, \
    postgres_port


def parse_arguments() -> argparse.Namespace:
    """
    Parses the arguments feeded to the program.

    Returns:
    arguments   -- Namespace with the arguments feeded to the program
    """
    parser = argparse.ArgumentParser(
        description="Second step of the ETL pipeline at Bargainista."
    )
    parser.add_argument(
        "--city", default="valencia", type=str,
        help="Name of the city where flats are to be retrieved."
    )

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_arguments()

    # Connect to the store database
    storer = BargainistaMongoHandler(
        mongo_host, mongo_port, mongo_user, mongo_pass, mongo_auth_db,
        args.city
    )

    # Connect to the Postgres database
    database = BargainistaPostgresHandler(
        postgres_host, postgres_port, postgres_user, postgres_pass,
        postgres_db, args.city
    )

    # Get the latest flat in the postgres database
    latest_flat = database.get_latest_flat()

    # Load all flats newer than the latest
    new_flats = storer.retrieve_new_flats(latest_flat)

    # Perform health checks and insert it into the Postgres
    for flat in new_flats:
        if not flat_health_check(flat):
            continue
        clean_flat = clean_flat_data(flat)
        database.create_flat_attributtes(clean_flat)  # Create attributes
        # before checking
        if not database.duplicated_flat(clean_flat):
            database.insert_flat(clean_flat)

    # Close the connection to the databases
    del(storer, database)
