import argparse

import category_encoders
import xgboost
from credentials import (mongo_auth_db, mongo_host, mongo_pass, mongo_port,
                         mongo_user, postgres_db, postgres_host, postgres_pass,
                         postgres_port, postgres_user)
from MongoHandler import BargainistaMongoHandler
from PostgresHandler import BargainistaPostgresHandler
from Predictor import BargainistaPredictor


def parse_arguments() -> argparse.Namespace:
    """
    Parses the arguments feeded to the program.

    Returns:
    arguments   -- Namespace with the arguments feeded to the program
    """
    parser = argparse.ArgumentParser(
        description="Predictor of market prices for properties at Bargainista."
    )
    parser.add_argument(
        "--city", default="valencia", type=str,
        help="Name of the city where flats are to be retrieved."
    )

    return parser.parse_args()


if __name__ == "__main__":
    # Parse the arguments
    args = parse_arguments()

    # Initialize the connections to the databases
    database = BargainistaPostgresHandler(
        postgres_host, postgres_port, postgres_user, postgres_pass,
        postgres_db, args.city
    )
    mongo_store = BargainistaMongoHandler(
        mongo_host, mongo_port, mongo_user, mongo_pass, mongo_auth_db,
        args.city
    )

    # Retrieve the latest trained model
    predictor = BargainistaPredictor(mongo_store)

    # Retrieve the flats without a market price
    flats = database.get_new_flats()

    if len(flats) > 0:
        # Predict their prices
        flats["market_price"] = predictor.predict(flats)

        # Insert the market prices into the database
        for index, flat in flats.iterrows():
            database.set_market_price(flat["id"], flat["market_price"])

    # Close the open connections
    del(database)
    del(mongo_store)
    del(predictor)
