import argparse
import numpy
import time
from typing import List, Tuple

from Extractor import BargainistaExtractor
from MongoHandler import BargainistaMongoHandler

from credentials import key, secret, mongo_user, mongo_pass, mongo_auth_db, \
    mongo_host, mongo_port


def parse_arguments() -> argparse.Namespace:
    """
    Parses the arguments feeded to the program.

    Returns:
    arguments   -- Namespace with the arguments feeded to the program
    """
    parser = argparse.ArgumentParser(
        description="First step of the ETL pipeline at Bargainista."
    )
    parser.add_argument(
        "--city", default="valencia", type=str,
        help="Name of the city where flats are to be retrieved."
    )
    parser.add_argument(
        "--max_pages", default=20, type=int,
        help="Maximum number of pages (50 flats per page) to be retrieved."
    )

    return parser.parse_args()


def set_city(city: str) -> Tuple[float, float, int]:
    """
    Sets the correct coordinates to perform an API call in a given city.

    Keyword arguments:
    city    -- Name of the city

    Returns:
    altitude    -- altitude of the city center
    longitude   -- longitude of the city center
    radius      -- radius to consider in the property search around the
                   city center
    """
    if city == "valencia":
        altitude: float = 39.47
        longitude: float = -0.376389
        radius: float = 6000
    else:
        raise ValueError(
            f"{args.city} not available, please choose another city."
        )

    return (altitude, longitude, radius)


def get_flats(storer: BargainistaMongoHandler,
              extractor: BargainistaExtractor,
              max_pages: int) -> list:
    """
    Performs calls over the idealista API until either all flats are
    parsed, the limit is reached, or we reach the flats from the
    last time we ran the query.

    Keyword arguments:
    storer      -- BargainistaMongoDB storer instance to check the last
                   saved properties
    extractor   -- BargainistaExtractor instance to retrieve the data
    max_pages   -- Maximum API calls allowed

    Returns:
    flats   -- List of dictionaries holding the property information
    """
    latest_flats = storer.retrieve_latest(50)

    flats, pages = extractor.get_flats(1)
    print(f"There are around {50*pages} flats in the query.")
    pages = min(max_pages, pages)
    print(f"Considering up to {50*(pages)} flats.")

    # Retrieve flats until finding one that is already in the data store
    for page in range(2, pages+1):
        more_flats, _ = extractor.get_flats(page)
        flats += more_flats
        if any(numpy.isin(get_flat_ids(flats), latest_flats)):
            print(f"At page {page} found flats already in the database.")
            break

    return flats


def get_flat_ids(flats: List[dict]) -> List[int]:
    """
    Get a list of the ids of the flats provided.

    Keyword arguments:
    flats   -- List of dictionaries holding the property information

    Returns:
    ids     -- List of the ids of the properties
    """
    ids = []

    for flat in flats:
        ids.append(flat["propertyCode"])

    return ids


def load_flats(storer: BargainistaMongoHandler, flats: List[dict]):
    """
    Timestamp and save the retrieved flats into the MongoDB database

    Keyword arguments:
    storer  -- BargainistaMongoDB storer instance to save the items
    flats   -- List of dictionaries holding the property information
    """
    counter: int = 0

    for flat in flats:
        flat["timestamp"] = time.time()
        if storer.insert_flat(flat):
            counter += 1

    print(f"Inserted {counter} records.")


if __name__ == "__main__":
    # Parse the arguments and set the city parameters
    args = parse_arguments()
    city_params = set_city(args.city)

    # Connect to the store database
    storer = BargainistaMongoHandler(mongo_host, mongo_port, mongo_user,
                                     mongo_pass, mongo_auth_db, args.city)

    # Initialize the API connector
    extractor = BargainistaExtractor(key, secret, city_params)

    # Get the flats
    flats = get_flats(storer, extractor, args.max_pages)

    # Load them into the database
    load_flats(storer, flats)

    # Close the connection to the database
    del(storer)
