import dash
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
import dash_html_components as html
import numpy
import plotly.graph_objs as go

from PostgresHandler import BargainistaPostgresHandler

from credentials import postgres_user, postgres_pass, postgres_db, \
    postgres_host, postgres_port, mapbox_access_token

# Initialize dash application
app = dash.Dash(
    __name__,
    meta_tags=[
        {
            "name": "viewport",
            "content": "width=device-width, initial-scale=1"
        }
    ]
)
app.title = "Bargainista - Listing of the best property bargains in Valencia"
server = app.server

# Initialize database connection
database = BargainistaPostgresHandler(
    postgres_host, postgres_port, postgres_user, postgres_pass,
    postgres_db, "valencia"
)

# Get cities and neighborhoods
neighborhoods = database.get_neighborhoods()


def generate_table(data):
    columns = ["id", "price", "savings", "savings_percent", "type", "floor",
               "size", "rooms", "bathrooms", "neighborhood", "city"]
    columns_label = ["Id", "Price (€)", "Savings (€)", "Savings (%)", "Type",
                     "Floor", "Size (m²)", "Rooms", "Bathrooms",
                     "Neighborhood", "City"]

    data["savings"] = calculate_savings(data["price"], data["discount"])
    data["savings"] = data["savings"].apply(price_to_str)
    data["savings_percent"] = data["discount"].astype(int)
    data["price"] = data["price"].apply(price_to_str)

    data.replace({"flat": "Flat", "chalet": "Chalet", "studio": "Studio",
                  "duplex": "Duplex", "countryHouse": "House",
                  "penthouse": "Attic"}, inplace=True)

    def write_cell(data, index, col):
        if col == "id":
            return html.A(
                f"{data.iloc[index][col]}",
                href=f"https://www.idealista.com/inmueble/{data.iloc[index][col]}/",
                target="_blank")
        else:
            return data.iloc[index][col]

    return html.Table([
        html.Thead(
            html.Tr([html.Th(col.capitalize())
                     for col in columns_label], style={"white-space": "nowrap"})
        ),
        html.Tbody([
            html.Tr([
                html.Td(write_cell(data, i, col), style={"white-space": "nowrap"}) for col in columns
            ]) for i in range(min(len(data), 25))
        ])
    ])


def generate_figure(data):
    fig = go.Figure(
        go.Scattermapbox(
            mode="markers",
            lon=data["longitude"],
            lat=data["latitude"],
            marker={
                "showscale": True
            }
        )
    )

    fig.update_layout(
        clickmode='event+select',
        mapbox={
            'accesstoken': mapbox_access_token,
            'style': 'basic',
            'zoom': 11,
            'center': go.layout.mapbox.Center(
                lat=39.47,
                lon=-0.376389
            )},
        autosize=True,
        margin={'t': 0, 'b': 0, 'l': 0, 'r': 0},
    )

    fig.update_traces(
        customdata=data,
        hovertemplate="<b>id: %{customdata[0]}</b><br>" +
        "price: %{customdata[1]} €<br>" +
        "size: %{customdata[5]} m²<extra></extra>",
        marker_symbol="circle",
        marker_size=15,
        marker_color=data["discount"],
        marker_colorbar_title="Discount",
        marker_reversescale=False
    )

    return fig


def price_to_str(price):
    return "{:,.0f}".format(price).replace(',', ' ')


def price_to_str_eur(price):
    return "{:,.0f} €".format(price).replace(',', ' ')


def calculate_savings(price, discount):
    return 1000*round(price*discount/100000)


technical_info = """
1. An ETL pipeline that is divided in two steps.
    * The extracting process queries the idealista API and stores the last ads
      in a MongoDB instance.
    * The load process cleans and structures (getting rid of outliers and
      duplicated entries) the raw data before inserting it into a Postgres
      database.
2. A training step where the clean data is fed to an XGBoost regressor that
   learns how to predict the properties market price. Once the training is
   complete, the trained model is pickled and stored in the MongoDB instance.
3. A predicting step, where the latest trained model is retrieved and it is
   used to predict the market prices for the new property listings. These
   listings are updated in the Postgres database with their market price.
4. Last, an interactive dashboard built with Plotly Dash (Flask) retrieves
   the discounted properties from the Postgres instance and it serves the
   data in this web application.
"""

information = [
    html.P(
        """Select the points from the map to check out the
        best deals in real estate property, or navigate to the bottom of the
        page to check the top 25  featured bargains. Use the sliders below the
        map to fine tune the properties to be shown."""
    ),
    html.P(
        children=[
            """The algorithm behind the scenes takes all the information in the
            listings in """,
            html.A(
                "idealista.com",
                href="https://idealista.com",
                target="_blank"
            ),
            """ (size, floor, rooms, bathrooms, ...) to learn how to predict
            the market prices of the real estate property in Valencia. Then,
            those that have a price below market are listed in this page. """
        ]
    ),
]

technical_info = [
    html.P("""This project leverages Idealista’s API to query the listings in
              Valencia in the last days. The API replies a series of JSON
              objects that we directly store (without any kind of processing)
              in a MongoDB database. Then, these listings are sorted, cleaned,
              deduplicated and structured in a normalized relational schema
              before inserting them into a Postgres instance that acts as our
              data warehouse."""),
    html.P("""All these properties are then used to train a boosted decision
              tree (actually, an XGBoost regressor) that aims to predict
              property prices as a function of their attributes (size,
              location, whether it has an elevator, …). The latest trained 
              model is retrieved and used to find out the market price of the 
              latest listings, which is uploaded to the Postgres database to 
              complete the listing record. Then, by means of comparing the 
              sticker price with our prediction we can compute the property 
              discount, allowing us to tell apart bargains from overpriced 
              properties."""),
    html.P("""Finally, we show the discounted properties (those that have a
              higher market price than their sticker price) in this web
              dashboard built with Plotly’s Dash (which actually uses Flask
              under the hood)."""),
    html.P("""The pipeline is implemented as a series of GitLab CI/CD scheduled 
              jobs that take care of the extraction, trasformation, load, 
              training and prediction. And, the dashboard is deployed with
              gunicorn and an Apache httpd reverse proxy."""),
    html.P(
        children=[
            "More details on how this site is built are available at ",
            html.A(
                "gomis.xyz",
                href="https://blog.gomis.xyz/projects/data/devops/bargainista.html",
                target="_blank"
            ),
            ", to check out the code, visit ",
            html.A(
                "my GitLab repository",
                href="https://gitlab.com/pablogomis/bargainista",
                target="_blank"
            ),
            "."
        ]
    )
]

app.layout = html.Div(
    children=[
        html.Div(
            className="one column",
            children=html.P("")
        ),
        html.Div(
            className="ten columns",
            children=[
                html.Div(
                    children=[
                        html.Div(
                            children=[
                                html.H1("bargainista"),
                                html.H6(
                                    children=[
                                        "A data science project by ",
                                        html.A(
                                            "Pablo Gomis",
                                            href="https://blog.gomis.xyz/about",
                                            target="_blank"
                                        ),
                                        "."
                                    ]
                                )
                            ]
                        ),
                    ]
                ),
                html.Div(
                    className="twelve columns",
                    children=[
                        html.Div(
                            children=[
                                html.Div(
                                    information
                                ),
                                html.Div(
                                    id="toggle-info",
                                    n_clicks=0,
                                    style={
                                        "text-decoration": "underline"
                                    }
                                ),
                                html.Div(
                                    id="info-container",
                                    children=technical_info
                                )
                            ],
                            style={
                                "text-align": "justify"
                            },
                        )
                    ],
                    style={
                        "padding-right": "2rem"
                    },
                ),
                html.Div(
                    children=[
                        html.Div(
                            className="twelve columns",
                            children=[
                                html.Div(
                                    className="eight columns",
                                    children=[
                                        html.H5("Valencia deals"),
                                        dcc.Graph(
                                            id="bargain-map",
                                            style={
                                                "padding-bottom": "1rem"
                                            }
                                        ),
                                    ]
                                ),
                                html.Div(
                                    className="four columns",
                                    children=[
                                        html.Center(
                                            html.H5(id="bargain-title")
                                        ),
                                        html.Div(
                                            id="bargain-data"
                                        )
                                    ],
                                    style={
                                        "padding-left": "2rem",
                                        "padding-right": "2rem",
                                        "vertical-align": "center"
                                    }
                                ),
                            ],
                            style={
                                "padding-top": "2rem",
                            }
                        ),
                        html.Div(
                            children=[

                                html.P(
                                    id="week-slider-header"),
                                dcc.Slider(
                                    id='week-slider',
                                    min=1,
                                    max=4,
                                    value=1,
                                    step=None,
                                    marks={
                                        1: "1",
                                        2: "2",
                                        3: "3",
                                        4: "4"
                                    }
                                ),
                                html.Div(
                                    id="sliders-container",
                                    children=[
                                        html.P(
                                            id="discount-slider-header"
                                        ),
                                        dcc.Slider(
                                            id='discount-slider',
                                            min=0,
                                            max=50,
                                            value=0,
                                            step=1,
                                            marks={
                                                0: "0",
                                                10: "10%",
                                                20: "20%",
                                                30: "30%",
                                                40: "40%",
                                                50: "50%"
                                            }
                                        ),
                                        html.P(
                                            id="price-slider-header"
                                        ),
                                        dcc.RangeSlider(
                                            id='price-slider',
                                            min=0,
                                            max=500000,
                                            step=5000,
                                            value=[0, 500000],
                                            marks={
                                                0: "0",
                                                100000: "100k",
                                                200000: "200k",
                                                300000: "300k",
                                                400000: "400k",
                                                500000: "all"
                                            }
                                        ),
                                        html.P(
                                            id="size-slider-header"
                                        ),
                                        dcc.RangeSlider(
                                            id="size-slider",
                                            min=0,
                                            max=300,
                                            step=5,
                                            value=[0, 300],
                                            marks={
                                                0: "0",
                                                50: "50",
                                                100: "100",
                                                200: "200",
                                                300: "all"
                                            }
                                        ),
                                        html.Div(
                                            dcc.Dropdown(
                                                id="selected-cities",
                                                options=[
                                                    {'label': i, 'value': i}
                                                    for i in neighborhoods["city"].unique()
                                                ],
                                                multi=True,
                                                placeholder='Filter by city...',
                                            ),
                                            style={
                                                "padding-top": "1rem",
                                                "padding-bottom": "1rem"
                                            }
                                        ),
                                        dcc.Dropdown(
                                            id="selected-neighborhoods",
                                            multi=True,
                                            placeholder='Filter by neighborhood...'
                                        ),
                                    ]
                                ),
                                html.Center(
                                    html.Button(
                                        id='sliders-button',
                                        n_clicks=0
                                    ),
                                    style={
                                        "padding-top": "2rem"
                                    }
                                )
                            ],
                        ),
                    ],
                ),
                html.Div(
                    className="twelve columns",
                    children=[
                        html.Center(
                            html.Div(
                                children=[
                                    html.H4("Featured bargains"),
                                    html.Div(
                                        className="nowrap",
                                        id='table-content',
                                        style={
                                            "table-layout": "fixed",
                                            "overflow-x": "auto",
                                            "whitespace": "nowrap"
                                        }
                                    )
                                ]
                            )
                        ),
                    ],
                    style={
                        "padding-top": "2rem",
                        "padding-bottom": "4rem"
                    }
                )
            ]
        ),
        html.Div(
            className="one column",
            children=html.P("")
        ),
    ]
)


@app.callback(
    Output('info-container', 'style'),
    Output('toggle-info', 'children'),
    Input('toggle-info', 'n_clicks')
)
def toggle_information(clicks):
    if clicks % 2:
        return ({'display': 'block'}, ["Show less..."])
    else:
        return ({'display': 'none'}, ["Show more..."])


@app.callback(
    Output('sliders-container', 'style'),
    Output('sliders-button', 'children'),
    Input('sliders-button', 'n_clicks')
)
def toggle_sliders(clicks):
    if clicks % 2:
        return ({'display': 'block'}, "Hide additional tunable controls")
    else:
        return ({'display': 'none'}, "Show additional tunable controls")


@ app.callback(
    Output('week-slider-header', 'children'),
    Input('week-slider', 'value')
)
def update_week_slider_header(value):
    if value == 1:
        return "Weeks of listings (showing last week's)"
    else:
        return f"Weeks of listings (showing last {value} weeks)"


@ app.callback(
    Output('discount-slider-header', 'children'),
    Input('discount-slider', 'value')
)
def update_bargain_slider_header(value):
    return f"Minimum discount (considering properties over {value}%)"


@ app.callback(
    Output('price-slider-header', 'children'),
    Input('price-slider', 'value')
)
def update_price_slider_header(value):
    if value[0] == 0 and value[1] == 500000:
        return "Price range (including all properties)"
    elif value[1] == 500000:
        return f"Price range (starting at {value[0]} €)"
    else:
        return f"Price range (between {value[0]} and {value[1]}€)"


@ app.callback(
    Output('size-slider-header', 'children'),
    Input('size-slider', 'value')
)
def update_size_slider_header(value):
    if value[0] == 0 and value[1] == 300:
        return "Size range (including all properties)"
    elif value[1] == 300:
        return f"Size range (starting at {value[0]} m²)"
    else:
        return f"Size range (between {value[0]} and {value[1]} m²)"


@ app.callback(
    Output('selected-neighborhoods', 'options'),
    Input('selected-cities', 'value')
)
def update_neighborhood_dropdown(value):
    if value is not None:
        if len(value) != 0:
            city_hoods = neighborhoods[numpy.isin(
                neighborhoods["city"], value)]
        else:
            city_hoods = neighborhoods
    else:
        city_hoods = neighborhoods

    return [{'label': i, 'value': i} for i in city_hoods["neighborhood"]]


@ app.callback(
    Output('bargain-map', 'figure'),
    Output('table-content', 'children'),
    Input('week-slider', 'value'),
    Input('discount-slider', 'value'),
    Input('price-slider', 'value'),
    Input('size-slider', 'value'),
    Input('selected-cities', 'value'),
    Input('selected-neighborhoods', 'value')
)
def display_map_and_table(week, min_discount, price_range, size_range,
                          selected_cities, selected_neighborhoods):
    # Retrieve and clean the data
    data = database.get_discounted_flats(week)

    data = data[data["discount"] > min_discount]

    if price_range[1] == 500000:
        data = data[data["price"] > price_range[0]]
    else:
        data = data[data["price"] > price_range[0]]
        data = data[data["price"] < price_range[1]]

    if size_range[1] == 300:
        data = data[data["size"] > size_range[0]]
    else:
        data = data[data["size"] > size_range[0]]
        data = data[data["size"] < size_range[1]]

    if selected_cities is not None:
        if len(selected_cities) != 0:
            data = data[numpy.isin(data["city"], selected_cities)]

    if selected_neighborhoods is not None:
        if len(selected_neighborhoods) != 0:
            data = data[numpy.isin(data["neighborhood"],
                                   selected_neighborhoods)]

    return generate_figure(data), generate_table(data)


@ app.callback(
    Output('bargain-title', 'children'),
    Output('bargain-data', 'children'),
    Input('bargain-map', 'clickData')
)
def display_bargain(clickData):
    if clickData is None:
        return "", ""
    else:
        data = clickData["points"][0]["customdata"]
        title = f"{data[3].capitalize()} in {data[9]} ({data[8]})"
        savings = price_to_str_eur(calculate_savings(data[1], data[2]))
        discount = int(data[2])
        info = [
            html.Div(
                className="six columns",
                children=[
                    html.H6("Idealista id"),
                    html.P(
                        html.A(
                            f"{data[0]}",
                            href=f"https://www.idealista.com/inmueble/{data[0]}/",
                            target="_blank"
                        )
                    ),
                    html.H6("Price"),
                    html.P(price_to_str_eur(data[1])),
                    html.H6("Floor"),
                    html.P(f"{data[4]}"),

                ]
            ),
            html.Div(
                className="six columns",
                children=[
                    html.H6("Size"),
                    html.P(f"{data[5]} m²"),
                    html.H6("Rooms"),
                    html.P(f"{data[6]}"),
                    html.H6("Bathrooms"),
                    html.P(f"{data[7]}"),

                ]
            ),
            html.Div(
                className="twelve columns",
                children=[
                    html.H6(html.B("Save up to")),
                    html.P(
                        html.B(savings+f' ({discount}%)')
                    )
                ]
            )
        ]

        return title, html.Center(info)


    # Running the server
if __name__ == "__main__":
    app.run_server(debug=True)
