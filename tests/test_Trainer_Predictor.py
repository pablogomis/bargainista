import json
import pandas
import pytest
import time


@pytest.fixture(scope="module")
def mongo():
    from MongoHandler import BargainistaMongoHandler
    from credentials import mongo_user, mongo_pass, mongo_auth_db, \
        mongo_host, mongo_port

    city = "dummy_city"

    return BargainistaMongoHandler(mongo_host, mongo_port, mongo_user, mongo_pass,
                                   mongo_auth_db, city)


@pytest.fixture(scope="module")
def postgres():
    from PostgresHandler import BargainistaPostgresHandler
    from credentials import postgres_user, postgres_pass, postgres_db, \
        postgres_host, postgres_port

    city = "dummy_city"

    return BargainistaPostgresHandler(
        postgres_host, postgres_port, postgres_user, postgres_pass,
        postgres_db, city
    )


def test_insert_properties(postgres):
    from Transformator import clean_flat_data, flat_health_check

    # Load 50 sample flats
    with open("tests/test_sample.json", "r") as file_handle:
        flats = json.loads(file_handle.read())

    for flat in flats:
        flat["timestamp"] = time.time()  # Append timestamp to mimick mongos
        if not flat_health_check(flat):
            continue
        clean_flat = clean_flat_data(flat)
        postgres.create_flat_attributtes(clean_flat)  # Create attributes
        # before checking
        if not postgres.duplicated_flat(clean_flat):
            postgres.insert_flat(clean_flat)

    assert len(postgres.get_new_flats()) == 48


def test_train(postgres, mongo):
    from ModelTrainer import BargainistaModelTrainer
    model_trainer = BargainistaModelTrainer(
        postgres,
        mongo,
        0.8,
        1)
    model_trainer.train_model()
    model_trainer.save_model()
    parameters = model_trainer._model.get_params()
    assert parameters["n_estimators"] == 200


def test_prediction(postgres, mongo):
    from Predictor import BargainistaPredictor
    predictor = BargainistaPredictor(mongo)
    flats = postgres.get_new_flats()

    flats["market_price"] = predictor.predict(flats)
    for _, flat in flats.iterrows():
        postgres.set_market_price(flat["id"], flat["market_price"])
    assert(flats["market_price"].mean() > 0.5 * flats["price"].mean())
    assert(flats["market_price"].mean() < 2 * flats["price"].mean())


def test_get_discounted_properties(postgres):
    data = postgres.get_discounted_flats(1)
    assert abs(data["discount"].mean()) > 0
    assert abs(data["discount"].mean()) < 10


def test_remove_dummies(postgres, mongo):
    from credentials import postgres_user, postgres_pass, postgres_db, \
        postgres_host, postgres_port
    postgres._connect(postgres_host, postgres_port, postgres_user,
                      postgres_pass, postgres_db)
    cursor = postgres._get_cursor()
    cursor.execute("DROP DATABASE dummy_city")
    cursor.close()

    mongo._store.drop()
    mongo._model_store.drop()

    assert "dummy_city" not in postgres._get_dbs()
    assert mongo._store.estimated_document_count() == 0
    assert mongo._model_store.estimated_document_count() == 0
