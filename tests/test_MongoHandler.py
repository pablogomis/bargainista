import pytest
import time


@pytest.fixture(scope="module")
def mongo():
    from MongoHandler import BargainistaMongoHandler
    from credentials import mongo_user, mongo_pass, mongo_auth_db, \
        mongo_host, mongo_port

    city: str = "dummy_city"

    return BargainistaMongoHandler(mongo_host, mongo_port, mongo_user, mongo_pass,
                                   mongo_auth_db, city)


def test_connection(mongo):
    server_info = mongo._mongo_client.server_info()
    assert server_info["ok"] == 1.0


def test_insert_dummy(mongo):
    dummy_flat = {
        "propertyCode": "0",
        "price": 1000000,
        "timestamp": time.time()
    }
    assert mongo.insert_flat(dummy_flat) == True


def test_read_dummy(mongo):
    dummy_flat = mongo.retrieve_flat("0")
    assert dummy_flat["price"] == 1000000


def test_retrieve_latest(mongo):
    latest_flats = mongo.retrieve_latest(10)
    assert len(latest_flats) == 1
    assert latest_flats == sorted(latest_flats, reverse=True)


def test_retrieve_new_flats(mongo):
    new_flats = mongo.retrieve_new_flats(time.time()-100)
    for flat in new_flats:
        assert flat["propertyCode"] == "0"


def test_remove_dummy(mongo):
    delete_result = mongo._store.delete_one({"propertyCode": "0"})
    assert delete_result.deleted_count == 1
