import pytest
import time
from Transformator import clean_flat_data, flat_health_check


@pytest.fixture(scope="module")
def flat():
    return {
        "propertyCode": "91879373",
        "thumbnail": "https://img3.idealista.com/blur/WEB_LISTING/0/id.pro.es.image.master/b6/44/30/825468254.jpg",
        "externalReference": "015PAB165-B",
        "numPhotos": 36,
        "floor": "3",
        "price": 164990.0,
        "propertyType": "flat",
        "operation": "sale",
        "size": 95.0,
        "exterior": True,
        "rooms": 3,
        "bathrooms": 2,
        "address": "Paiporta",
        "province": "Val\u00e8ncia",
        "municipality": "Paiporta",
        "country": "es",
        "latitude": 39.4319271,
        "longitude": -0.4088607,
        "showAddress": False,
        "url": "https://www.idealista.com/inmueble/91879373/",
        "distance": "5068",
        "hasVideo": False,
        "status": "good",
        "newDevelopment": False,
        "hasLift": True,
        "parkingSpace": {
            "hasParkingSpace": True,
            "isParkingSpaceIncludedInPrice": True
        },
        "priceByArea": 1737.0,
        "detailedType": {
            "typology": "flat"
        },
        "suggestedTexts": {
            "subtitle": "Paiporta",
            "title": "Piso"
        },
        "hasPlan": False,
        "has3DTour": False,
        "has360": False,
        "hasStaging": False,
        "topNewDevelopment": False,
        "timestamp": time.time()
    }


def test_flat_health_check():
    sane_flat = {
        "rooms": 3,
        "size": 100,
        "price": 80000
    }
    too_cheap_flat = {
        "rooms": 3,
        "size": 100,
        "price": 500
    }
    unrealistic_flat = {
        "rooms": 50,
        "size": 100,
        "price": 80000
    }

    assert flat_health_check(sane_flat)
    assert not flat_health_check(too_cheap_flat)
    assert not flat_health_check(unrealistic_flat)


def test_clean_flat_data(flat):
    clean_flat = clean_flat_data(flat)
    assert clean_flat["id"] == flat["propertyCode"]
    assert clean_flat["elevator"] == flat["hasLift"]
