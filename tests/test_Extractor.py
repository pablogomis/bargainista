import pytest


@pytest.fixture(scope="module")
def extractor():
    from Extractor import BargainistaExtractor
    from credentials import key, secret

    # The city of Valencia
    altitude: float = 39.47
    longitude: float = -0.376389
    radius: float = 6000

    return BargainistaExtractor(key, secret,
                                (altitude, longitude, radius))


def test_token_retrieval(extractor):
    assert len(extractor.token) == 291


# Beware of wasting too many API calls
"""def test_flat_retrieval(extractor):
    import json

    first_page, pages = extractor.get_flats(1)

    assert type(first_page) == list
    assert type(first_page[0] == dict)
    assert pages >= 1

    with open("test_sample.json", "w") as f:
        json.dump(first_page, f)"""
