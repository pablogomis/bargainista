import pytest
import time
import pandas


@pytest.fixture(scope="module")
def postgres():
    from PostgresHandler import BargainistaPostgresHandler
    from credentials import postgres_user, postgres_pass, postgres_db, \
        postgres_host, postgres_port

    city: str = "dummy_city"

    return BargainistaPostgresHandler(
        postgres_host, postgres_port, postgres_user, postgres_pass,
        postgres_db, city
    )


@pytest.fixture(scope="module")
def dummy_flat():
    flat = {
        "id": 1, "price": 100000, "size": 100, "rooms": 3, "bathrooms": 2,
        "floor": 5, "elevator": True, "type": "flat", "parking": True,
        "exterior": True, "status": True, "development": False,
        "neighborhood": "Benicalap", "district": "Benicalap",
        "city": "Valencia", "latitude": 5, "longitude": 6,
        "timestamp": time.time()
    }
    return flat


def test_connection(postgres):
    from credentials import postgres_user
    user = postgres._postgres_connector.get_parameter_status(
        "session_authorization"
    )

    assert user == postgres_user


def test_city_creation(postgres):
    assert "dummy_city" in postgres._get_dbs()


def test_insert_flat(postgres, dummy_flat):
    postgres.create_flat_attributtes(dummy_flat)
    postgres.insert_flat(dummy_flat)


def test_flat_duplicated(postgres, dummy_flat):
    assert postgres.duplicated_flat(dummy_flat)


def test_get_latest_flat(postgres, dummy_flat):
    assert dummy_flat["timestamp"] == postgres.get_latest_flat()


def test_get_all_flats(postgres, dummy_flat):
    flats = postgres.get_all_flats()
    assert len(flats) == 1
    assert type(flats) == pandas.DataFrame
    assert flats.loc[0, "price"] == dummy_flat["price"]


def test_get_new_flats(postgres, dummy_flat):
    flats = postgres.get_new_flats()
    assert len(flats) == 1
    assert type(flats) == pandas.DataFrame
    assert flats.loc[0, "price"] == dummy_flat["price"]


def test_get_discounted_flats(postgres, dummy_flat):
    postgres.set_market_price(dummy_flat["id"], dummy_flat["price"]+10000)
    discounted_flats = postgres.get_discounted_flats(1)
    assert discounted_flats.loc[0, "id"] == dummy_flat["id"]


def test_get_neighborhoods(postgres, dummy_flat):
    neighborhoods = postgres.get_neighborhoods()
    assert neighborhoods.loc[0, "neighborhood"] == dummy_flat["neighborhood"]


def test_remove_dummy_city(postgres):
    from credentials import postgres_user, postgres_pass, postgres_db, \
        postgres_host, postgres_port
    postgres._connect(postgres_host, postgres_port, postgres_user,
                      postgres_pass, postgres_db)
    cursor = postgres._get_cursor()
    cursor.execute("DROP DATABASE dummy_city")
    cursor.close()

    assert "dummy_city" not in postgres._get_dbs()
