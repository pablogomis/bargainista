from category_encoders import GLMMEncoder, OneHotEncoder
import pandas
import numpy
from sklearn.model_selection import train_test_split, GridSearchCV
import xgboost
from PostgresHandler import BargainistaPostgresHandler
from MongoHandler import BargainistaMongoHandler
from matplotlib import pyplot
import os
import pickle


class BargainistaModelTrainer():
    """
    Class to train a predictive model that maps flat's attributtes to
    a target price.
    """

    def __init__(self, database: BargainistaPostgresHandler,
                 mongo_store: BargainistaMongoHandler,
                 price_quartile_threshold: float,
                 threads: int) -> None:
        """
        BargainistaModelTrainer constructor. It connects to the 
        postgres database, retrieves all the flat's data and trains
        a boosted decision tree to predict the properties price. The
        model is then saved into the MongoDB store.

        Keyword arguments:
        database                    --  BargainistaPostgresHandler 
                                        instance used to used to 
                                        retrieve the flats data
        mongo_store                 --  MongoDB instance used to save
                                        the trained model
        price_quartile_threshold    --  Quartile threshold to exclude
                                        luxury flats from training
        """
        # Retrieve the data and remove high price "luxury" outliers
        data = database.get_all_flats()
        data = data[
            data["price"] < data["price"].quantile(price_quartile_threshold)
        ]

        # Store data and target separately
        target = data.loc[:, "price"]
        data = data.loc[:, data.columns != "price"]

        self._threads = threads

        data, self._onehot_encoder, self._glmm_encoder = \
            self._encode_data(data, target)

        self._train_data, self._test_data, self._train_target, \
            self._test_target = train_test_split(data, target)

        self._model = self._optimize_model_parameters()

        self._mongo_store = mongo_store

    def _encode_data(self,
                     data: pandas.DataFrame,
                     target: pandas.DataFrame) -> pandas.DataFrame:
        """
        Encodes the categorical features into a series of binary 
        features for the property type, and a float in the case of 
        neighborhood, district and city.

        Keyword arguments:
        data            --  Pandas dataframe with all the features of 
                            the real estate properties.
        target          --  Pandas dataframe with their asking price.

        Returns:
        encoded_data    --  Pandas dataframe with the categorical
                            features encoded.
        """
        onehot_encoder = OneHotEncoder(cols=["type"])
        onehot_encoder.fit(data, target)
        data = onehot_encoder.transform(data)

        glmm_encoder = GLMMEncoder(randomized=True)
        glmm_encoder.fit(data, target)
        data = glmm_encoder.transform(data)
        return data, onehot_encoder, glmm_encoder

    def _optimize_model_parameters(self) -> xgboost.XGBRegressor:
        """
        Runs a grid cross-validation search over the most relevant 
        xgboost parameters (max_depth and learning_rate) to find the 
        values that better fit the train data.

        Returns:
        model   --  Best performing model in the grid search.
        """
        model = xgboost.XGBRegressor()

        cr_params_searcher = GridSearchCV(model, {
            "n_estimators": [200],
            "max_depth": numpy.linspace(2, 10, 9, dtype=int).tolist(),
            "learning_rate": numpy.linspace(0.05, 0.5, 10).tolist(),
            "n_jobs": [1]
        }, verbose=1, n_jobs=self._threads)

        # Choosing rmsle to minimize error relative to the asking price
        cr_params_searcher.fit(self._train_data, self._train_target,
                               eval_metric="rmsle")

        return cr_params_searcher.best_estimator_

    def train_model(self) -> None:
        """
        Trains the model with the best parameters on the training data.
        It uses the test data to monitor for overfitting. The model
        iteration stops once the rmsle stops improving in the test set.
        """
        self._model.fit(
            self._train_data, self._train_target, early_stopping_rounds=10,
            eval_metric="rmsle",
            eval_set=[
                (self._train_data, self._train_target),
                (self._test_data, self._test_target)
            ]
        )

    def debug_model(self, output_path: str) -> None:
        """
        Obtain information about the model's predictions such as the
        distribution of the discount (the ratio between the asking price
        and market price) in both text (quartiles) and histogram forms.
        It also plots the most relevant features for the model.

        Keyword arguments:
        output_path     --  Path to store the debugging information.
        """
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        def calculate_ratio(data, target):
            market_price = self._model.predict(
                data,
                ntree_limit=self._model.best_ntree_limit
            )
            discount = 100 * (1 - target/market_price)
            return discount.rename('discount')

        ratio_train = calculate_ratio(self._train_data, self._train_target)
        ratio_test = calculate_ratio(self._test_data, self._test_target)

        # Create and format a histogram
        n_bins: int = 25
        ratio_train.hist(bins=n_bins,
                         label=f"Train set ({ratio_train.count()} samples)")
        ratio_test.hist(bins=n_bins,
                        label=f"Test set ({ratio_test.count()} samples)")
        pyplot.title(f"Real estate discount distribution ({n_bins} bins)")
        pyplot.xlabel("Discount (%)")
        pyplot.ylabel("Events (#)")
        pyplot.legend(loc='best')

        pyplot.savefig(os.path.join(output_path, "discount_distribution.png"))

        # Get some stats about the discount distributions
        stats_output = '-----    Train dataset discount  -----\n'
        stats_output += str(ratio_train.describe()) + '\n'
        stats_output += '-----    Test dataset discount   -----\n'
        stats_output += str(ratio_test.describe())

        with open(os.path.join(output_path, "discount_stats.txt"),
                  "w+") as file_handle:
            file_handle.write(stats_output)

        # Save the figure showing the most important features
        xgboost.plot_importance(self._model)
        pyplot.tight_layout()
        pyplot.savefig(os.path.join(output_path, "feature_importance.png"))

    def save_model(self) -> bool:
        """
        Saves the encoders and the model in pickled form into the 
        MongoDB instance.

        Returns
        success -- True if writting succeded, False if it didn't
        """
        saved_model = pickle.dumps(self._model)
        saved_onehot = pickle.dumps(self._onehot_encoder)
        saved_glmm = pickle.dumps(self._glmm_encoder)

        return self._mongo_store.save_model(saved_model,
                                            saved_onehot,
                                            saved_glmm)
