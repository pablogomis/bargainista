def clean_flat_data(flat: dict) -> dict:
    """
    Takes the raw data from the idealista API, cleanses it and returns
    it the proper format to input it in the Postgres database.

    Keyword arguments:
    flat        -- Dictionary with the raw flat data from idealista

    Returns:
    clean_flat  -- Dictionary with the clean flat data
    """
    clean_flat = {
        "id": flat["propertyCode"],
        "price": flat["price"],
        "size": flat["size"],
        "rooms": flat.get("rooms", 1),
        "bathrooms": flat.get("bathrooms", 1),
        "type": flat["propertyType"],
        "floor": flat.get("floor", "0"),
        "elevator": flat.get("hasLift", "false"),
        "status": flat.get("status", "") == "good",
        "exterior": flat.get("exterior", "false"),
        "parking": flat.get("parkingSpace", dict()).get(
            "isParkingSpaceIncludedInPrice", "false"),
        "development": flat.get("newDevelopment", "false"),
        "city": flat["municipality"],
        "district": flat.get("district", flat.get("municipality")),
        "neighborhood": flat.get("neighborhood", flat.get("district")),
        "latitude": flat["latitude"],
        "longitude": flat["longitude"],
        "timestamp": flat["timestamp"],
    }

    if clean_flat["floor"].isdigit():
        clean_flat["floor"] = int(clean_flat["floor"])
    else:
        clean_flat["floor"] = 0

    if clean_flat["neighborhood"] is None:
        clean_flat["neighborhood"] = clean_flat["district"]

    return clean_flat


def flat_health_check(flat: dict) -> bool:
    """
    Performs a series of sanity checks on the property to avoid 
    including outliers into the database.

    Keyword arguments:
    flat        -- Dictionary with the raw flat data from idealista

    Returns:
    healthy     -- True if the property seems legit
    """

    price_per_sqm = flat["price"]/flat["size"]
    if flat["rooms"] == 0:
        sqm_per_rooms = flat["size"]
    else:
        sqm_per_rooms = flat["size"]/flat["rooms"]

    if (price_per_sqm > 25000) or (price_per_sqm < 250):
        return False
    if (sqm_per_rooms < 10) or (sqm_per_rooms > 100):
        return False

    return True
