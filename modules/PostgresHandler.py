import pandas
import psycopg2
from typing import List, Tuple, Dict


class BargainistaPostgresHandler():
    """
    Class to load and retrieve the clean flats data from the Postgres
    instance.
    """

    def __init__(self, host: str, port: int, username: str,
                 password: str, db: str, city: str) -> None:
        """
        BargainistaPostgresHandler constructor. It connects to the
        Postgres database, allowing to insert and retrieve data from it.

        Keyword arguments:
        host        -- Postgres server hostname
        port        -- Postgres server port
        username    -- Postgres user
        password    -- Postgres user's password
        db          -- Postgres database to be accessed
        city        -- Name of the city database
        """
        self._host = host
        self._port = port
        self._username = username
        self._password = password

        # Connect to the database
        self._connect(self._host, self._port,
                      self._username, self._password, db)

        # Check if the city exists, create it if not
        new_db = city not in self._get_dbs()
        if new_db:
            self._create_db(city)

        # Reconnect to the city database
        self._set_db(city)
        if new_db:
            self._set_up_tables()

        # Store the available districts and types
        self._neighborhoods = self._get_neighborhoods()
        self._types = self._get_types()

    def __del__(self) -> None:
        """
        BargainistaPostgresHandler destructor. It closes the connection
        to the Postgres database.
        """
        self._disconnect()

    # Methods to handle the db connections
    def _connect(self, host: str, port: int, username: str,
                 password: str, db: str) -> None:
        """
        Connects to the Postgres database, allowing to insert and
        retrieve data from it.

        Keyword arguments:
        host        -- Postgres server hostname
        port        -- Postgres server port
        username    -- Postgres user
        password    -- Postgres user's password
        db     -- Postgres database to be accessed
        """
        try:
            self._postgres_connector = psycopg2.connect(
                f"dbname={db} user={username} password={password} " +
                f"host={host} port={port}"
            )
            self._postgres_connector.get_parameter_status(
                "session_authorization"
            )
            self._postgres_connector.autocommit = True
        except psycopg2.OperationalError:
            raise Exception("Couldn't access the Postgress instance. " +
                            "Check conectivity.")

    def _disconnect(self) -> None:
        """
        Gracefully disconnects from the Postgres instance.
        """
        self._postgres_connector.close()

    def _get_cursor(self) -> psycopg2.extensions.cursor:
        """
        Creates a cursor in the current connection.

        Returns
        cursor  -- Connector cursor to address the database
        """
        return self._postgres_connector.cursor()

    def _get_dbs(self) -> List[str]:
        """
        Get a list of all the city databases.

        Returns:
        cities  -- List of all the city databases
        """
        cities = []
        default_dbs = ["bargainista", "postgres", "template0", "template1"]

        with self._get_cursor() as cursor:
            cursor.execute("SELECT datname FROM pg_database;")

            for record in cursor:
                if record[0] not in default_dbs:
                    cities.append(record[0])

        return cities

    def _create_db(self, city: str) -> bool:
        """
        Creates a database for the specified city.

        Keyword arguments:
        city    -- Name of the city database

        Returns:
        success -- True if the creation succeded, False if it didn't
        """
        with self._get_cursor() as cursor:
            cursor.execute(f"CREATE DATABASE {city};")

        if city in self._get_dbs():
            return True
        else:
            return False

    def _set_up_tables(self) -> None:
        """
        Create the database schema for the new city database.
        """
        with self._get_cursor() as cursor:
            cursor.execute(
                """CREATE TABLE types (
                    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                    type TEXT NOT NULL UNIQUE
                );
                CREATE TABLE cities(
                    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                    city TEXT NOT NULL UNIQUE
                );
                CREATE TABLE districts(
                    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                    district TEXT NOT NULL UNIQUE,
                    city SMALLINT NOT NULL,
                    CONSTRAINT fk_city
                        FOREIGN KEY(city)
                            REFERENCES cities(id)
                );
                CREATE TABLE neighborhoods(
                    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
                    neighborhood TEXT NOT NULL UNIQUE,
                    district SMALLINT NOT NULL,
                    CONSTRAINT fk_district
                        FOREIGN KEY(district)
                            REFERENCES districts(id)
                );
                CREATE TABLE estate (
                    id INT PRIMARY KEY,
                    price DOUBLE PRECISION NOT NULL,
                    market_price DOUBLE PRECISION DEFAULT NULL,
                    size REAL NOT NULL,
                    rooms SMALLINT NOT NULL,
                    bathrooms SMALLINT NOT NULL,
                    floor SMALLINT NOT NULL,
                    elevator BOOLEAN NOT NULL,
                    type SMALLINT NOT NULL,
                    parking BOOLEAN NOT NULL,
                    exterior BOOLEAN NOT NULL,
                    status BOOLEAN NOT NULL,
                    development BOOLEAN NOT NULL,
                    neighborhood SMALLINT NOT NULL,
                    latitude DOUBLE PRECISION NOT NULL,
                    longitude DOUBLE PRECISION NOT NULL,
                    timestamp DOUBLE PRECISION NOT NULL,
                    CONSTRAINT fk_type
                        FOREIGN KEY(type)
                            REFERENCES types(id),
                    CONSTRAINT fk_neighborhood
                        FOREIGN KEY(neighborhood)
                            REFERENCES neighborhoods(id)
                );"""
            )

    def _set_db(self, city: str) -> None:
        """
        Reconnects to the Postgres instance attaching to the city
        database.

        Keyword arguments:
        city    -- Name of the city database
        """
        self._disconnect()
        self._connect(self._host, self._port, self._username, self._password,
                      city)

    # Methods to retrieve current types and neighborhoods in the db
    def _get_types(self) -> Dict[str, int]:
        """
        Returns a dictionary with all the types available in the
        postgres instance.

        Returns:
        types   -- Dictionary with all the types and their indexes
        """
        types = dict()

        with self._get_cursor() as cursor:
            cursor.execute("SELECT id, type FROM types;")

            for record in cursor:
                types[record[1]] = record[0]

        return types

    def _get_neighborhoods(self) -> Dict[str, int]:
        """
        Returns a dictionary with all the neighborhoods available in the
        postgres instance.

        Returns:
        neighborhoods   -- Dictionary with all the neighborhoods and 
                           their indexes
        """
        neighborhood = dict()

        with self._get_cursor() as cursor:
            cursor.execute("SELECT id, neighborhood FROM neighborhoods;")

            for record in cursor:
                neighborhood[record[1]] = record[0]

        return neighborhood

    # Methods to create the missing types, neighborhoods, districts and
    # cities
    def _get_type(self, flat_type: str) -> int:
        """
        Get the type index.

        Keywords:
        type    -- Name of the property type

        Returns:
        id      -- Index of the type
        """
        with self._get_cursor() as cursor:
            cursor.execute("""
                SELECT id
                FROM types
                WHERE type = %s;""", (flat_type, ))
            answer = cursor.fetchone()

        if answer is None:
            answer = self._create_type(flat_type)

        return answer[0]

    def _create_type(self, flat_type: str) -> int:
        """
        Insert a new type to the database.

        Keywords:
        type    -- Name of the property type

        Returns:
        answer  -- Tuple with the index of the type
        """
        with self._get_cursor() as cursor:
            cursor.execute("""
                INSERT INTO types (type)
                VALUES (%s);""", (flat_type, ))
            cursor.execute("""
                SELECT id
                FROM types
                WHERE type = %s""", (flat_type, ))
            answer = cursor.fetchone()

        return answer

    def _get_neighborhood(self,
                          neighborhood: str,
                          district: str,
                          city: str) -> int:
        """
        Get the neighborhood index.

        Keywords:
        neighborhood    -- Name of the neighborhood
        city            -- Name of the city

        Returns:
        id          -- Index of the neighborhood
        """
        district_id = self._get_district(district, city)

        with self._get_cursor() as cursor:
            cursor.execute("""
                SELECT id
                FROM neighborhoods
                WHERE neighborhood = %s AND district = %s;""",
                           (neighborhood, district_id)
                           )
            answer = cursor.fetchone()

        if answer is None:
            answer = self._create_neighborhood(neighborhood, district_id)

        return answer[0]

    def _create_neighborhood(self,
                             neighborhood: str,
                             district_id: str) -> Tuple[int]:
        """
        Insert a new neighborhood to the database.

        Keywords:
        neighborhood    -- Name of the neighborhood
        district_id     -- Index of the district

        Returns:
        answer      -- Tuple with the index of the neighborhood
        """
        with self._get_cursor() as cursor:
            cursor.execute("""
                INSERT into neighborhoods (neighborhood, district)
                VALUES (%s, %s);""", (neighborhood, district_id))
            cursor.execute(
                """
                SELECT id
                FROM neighborhoods
                WHERE neighborhood = %s AND district = %s;""",
                (neighborhood, district_id)
            )
            answer = cursor.fetchone()

        return answer

    def _get_district(self, district: str, city: str) -> int:
        """
        Get the district index.

        Keywords:
        district    -- Name of the district
        city        -- Name of the city

        Returns:
        id          -- Index of the district
        """
        city_id = self._get_city(city)

        with self._get_cursor() as cursor:
            cursor.execute("""
                SELECT id
                FROM districts
                WHERE district = %s AND city = %s;""", (district, city_id))
            answer = cursor.fetchone()

        if answer is None:
            answer = self._create_district(district, city_id)

        return answer[0]

    def _create_district(self, district: str, city_id: str) -> Tuple[int]:
        """
        Insert a new district to the database.

        Keywords:
        district    -- Name of the district
        city_id     -- Index of the city

        Returns:
        answer      -- Tuple with the index of the district
        """
        with self._get_cursor() as cursor:
            cursor.execute("""
                INSERT into districts (district, city)
                VALUES (%s, %s);""", (district, city_id))
            cursor.execute("""
                SELECT id
                FROM districts
                WHERE district = %s AND city = %s;""", (district, city_id))
            answer = cursor.fetchone()

        return answer

    def _get_city(self, city: str) -> int:
        """
        Gets the city index.

        Keywords:
        city    -- Name of the city

        Returns:
        id      -- Index of the city
        """
        with self._get_cursor() as cursor:
            cursor.execute("""
                SELECT id
                FROM cities
                WHERE city = %s;""", (city, ))
            answer = cursor.fetchone()

        if answer is None:
            answer = self._create_city(city)

        return answer[0]

    def _create_city(self, city: str) -> Tuple[int]:
        """
        Insert a new city to the database.

        Keywords:
        city    -- Name of the city

        Returns:
        answer  -- Tuple with the index of the city
        """
        with self._get_cursor() as cursor:
            cursor.execute("""
                INSERT INTO cities (city)
                VALUES (%s);""", (city, ))
            cursor.execute("""
                SELECT id
                FROM cities
                WHERE city = %s""", (city, ))
            answer = cursor.fetchone()

        return answer

    # Public methods
    def create_flat_attributtes(self, flat: dict) -> None:
        """
        Ensures the type, neighborhood, disitrict and city of the flat
        are available in their dimension tables.

        Keyword arguments:
        flat    -- Dictionary with all the properties of the flat
        """
        # Check if neighborhood exists if not, create it
        if flat["neighborhood"] not in self._neighborhoods:
            neighborhood_index = self._get_neighborhood(
                flat["neighborhood"], flat["district"], flat["city"]
            )
            self._neighborhoods[flat["neighborhood"]] = neighborhood_index

        # Check if type exists if not, create it
        if flat["type"] not in self._types:
            type_index = self._get_type(flat["type"])
            self._types[flat["type"]] = type_index

    def duplicated_flat(self, flat: dict) -> bool:
        """
        Check if the flat is already in the database. We look for 
        properties with the same price, floor and rooms in the same 
        neighborhood. Or properties with very similar prices at the 
        same exact location.

        The ads have to be less than 30 days apart to be considered 
        duplicated.

        Keyword arguments:
        flat        -- Dictionary with all the properties of the flat

        Returns:
        duplicated  -- True if the flat is already in the database
        """
        # Assign district by their index
        neighborhood = self._neighborhoods[flat["neighborhood"]]

        with self._get_cursor() as cursor:
            cursor.execute(
                """
                SELECT id
                FROM estate
                WHERE ((price = %s
                    AND floor = %s
                    AND rooms = %s
                    AND bathrooms = %s
                    AND neighborhood = %s)
                    OR (price BETWEEN 0.9*%s AND 1.1*%s
                    AND floor = %s
                    AND latitude = %s
                    AND longitude = %s))
                    AND timestamp > (%s - 2592000)""",
                (flat["price"], flat["floor"], flat["rooms"], flat["bathrooms"],
                 neighborhood, flat["price"], flat["price"], flat["floor"],
                 flat["latitude"], flat["longitude"], flat["timestamp"])
            )
            answer = cursor.fetchone()

        if answer is None:
            return False

        else:
            return True

    def insert_flat(self, flat: dict) -> None:
        """
        Inserts a flat into the database.

        Keyword arguments:
        flat    -- Dictionary with all the properties of the flat
        """
        # Get indexes for their neighborhood and type
        neighborhood = self._neighborhoods[flat["neighborhood"]]
        flat_type = self._types[flat["type"]]

        # Insert the data into the database
        with self._get_cursor() as cursor:
            cursor.execute(
                """
                INSERT INTO estate (
                    id,
                    price,
                    size,
                    rooms,
                    bathrooms,
                    floor,
                    elevator,
                    type,
                    parking,
                    exterior,
                    status,
                    development,
                    neighborhood,
                    latitude,
                    longitude,
                    timestamp)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
                    %s, %s, %s, %s);""",
                (flat["id"], flat["price"], flat["size"], flat["rooms"],
                 flat["bathrooms"], flat["floor"], flat["elevator"],
                 flat_type, flat["parking"], flat["exterior"],
                 flat["status"], flat["development"], neighborhood,
                 flat["latitude"], flat["longitude"], flat["timestamp"])
            )

    def get_latest_flat(self) -> float:
        """
        Get the timestamp of the latest flat inserted into the 
        database.

        Returns:
        time    -- timestamp of the latest flat inserted,
                   0 if no records available
        """
        with self._get_cursor() as cursor:
            cursor.execute(
                """
                SELECT MAX(timestamp)
                FROM estate;
                """
            )
            answer = cursor.fetchone()

        if answer == None or answer[0] == None:
            return 0

        return answer[0]

    def get_all_flats(self) -> pandas.DataFrame:
        """
        Get all the flats inserted into the database, it also retrieves
        the median, mean and standard deviation of the price of a sqm
        in their neighborhoods.

        Returns:
        data    -- Pandas dataframe holding all the flat data
        """
        query = """
            CREATE OR REPLACE VIEW neighborhood_stats AS
            SELECT neighborhood,
                PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY price/size)
                    AS median_sqm_price,
                AVG(price/size) AS mean_sqm_price,
                STDDEV_POP(price/size) AS std_sqm_price
            FROM estate
            GROUP BY neighborhood;

            SELECT price,
                size,
                rooms,
                bathrooms,
                floor,
                elevator,
                types.type,
                parking,
                exterior,
                status,
                development,
                neighborhoods.neighborhood,
                neighborhood_stats.mean_sqm_price AS neighborhood_mean_sqm_price,
                neighborhood_stats.median_sqm_price AS neighborhood_median_sqm_price,
                neighborhood_stats.std_sqm_price AS neighborhood_std_sqm_price,
                districts.district,
                cities.city
            FROM estate
            INNER JOIN types ON estate.type = types.id
            INNER JOIN neighborhoods ON estate.neighborhood = neighborhoods.id
            INNER JOIN neighborhood_stats
                ON estate.neighborhood = neighborhood_stats.neighborhood
            INNER JOIN districts ON neighborhoods.district = districts.id
            INNER JOIN cities ON districts.city = cities.id;"""
        return pandas.read_sql(query, self._postgres_connector)

    def get_new_flats(self) -> pandas.DataFrame:
        """
        Get all the flats without a predicted market price, it also
        retrieves the median, mean and standard deviation of the price
        of a sqm in their neighborhoods.

        Returns:
        data    -- Pandas dataframe holding all the new flat data
        """
        query = """
            CREATE OR REPLACE VIEW neighborhood_stats AS
            SELECT neighborhood,
                PERCENTILE_DISC(0.5) WITHIN GROUP (ORDER BY price/size)
                    AS median_sqm_price,
                AVG(price/size) AS mean_sqm_price,
                STDDEV_POP(price/size) AS std_sqm_price
            FROM estate
            GROUP BY neighborhood;

            SELECT estate.id,
                price,
                size,
                rooms,
                bathrooms,
                floor,
                elevator,
                types.type,
                parking,
                exterior,
                status,
                development,
                neighborhoods.neighborhood,
                neighborhood_stats.mean_sqm_price AS neighborhood_mean_sqm_price,
                neighborhood_stats.median_sqm_price AS neighborhood_median_sqm_price,
                neighborhood_stats.std_sqm_price AS neighborhood_std_sqm_price,
                districts.district,
                cities.city
            FROM estate
            INNER JOIN types ON estate.type = types.id
            INNER JOIN neighborhoods ON estate.neighborhood = neighborhoods.id
            INNER JOIN neighborhood_stats
                ON estate.neighborhood = neighborhood_stats.neighborhood
            INNER JOIN districts ON neighborhoods.district = districts.id
            INNER JOIN cities ON districts.city = cities.id
            WHERE market_price is NULL;"""
        return pandas.read_sql(query, self._postgres_connector)

    def set_market_price(self, id: int, market_price: float) -> None:
        """
        Set the market price of a given property.

        Keyword arguments:
        id              -- Property id
        market_price    -- Predicted market price of the property
        """
        with self._get_cursor() as cursor:
            cursor.execute(
                """
                UPDATE estate
                SET market_price = %s
                WHERE id = %s;""",
                (market_price, id)
            )

    def get_discounted_flats(self, weeks: int) -> pandas.DataFrame:
        """
        Get all the discounted (price less than market price) properties
        in the last specified weeks.

        Keyword arguments:
        weeks   -- Weeks worth of data to be retrieved

        Returns:
        flats   -- Pandas dataframe with all the discounted properties
                   in the specified period
        """
        query = f"""
                SELECT estate.id,
                    price,
                    100 * (1 - price/market_price) as discount,
                    types.type,
                    floor,
                    size,
                    rooms,
                    bathrooms,
                    cities.city,
                    neighborhoods.neighborhood,
                    latitude,
                    longitude,
                    timestamp
                FROM estate
                INNER JOIN types on estate.type = types.id
                INNER JOIN neighborhoods on estate.neighborhood = neighborhoods.id
                INNER JOIN districts on neighborhoods.district = districts.id
                INNER JOIN cities on districts.city = cities.id
                WHERE 100 * (1 - price/market_price) > 0
                    AND timestamp > (select extract(epoch from now())) - 604800 * {weeks}
                    AND status = true
                ORDER BY discount DESC;"""
        return pandas.read_sql(query, self._postgres_connector)

    def get_neighborhoods(self) -> pandas.DataFrame:
        """
        Get a list of all the neighborhoods and their cities.

        Returns:
        data    -- Pandas dataframe listing all neighborhoods and cities
        """
        query = """
                SELECT neighborhood,
                    cities.city
                FROM neighborhoods
                INNER JOIN districts ON neighborhoods.district = districts.id
                INNER JOIN cities ON districts.city = cities.id;"""
        return pandas.read_sql(query, self._postgres_connector)
