import category_encoders
import pandas
import pickle
import xgboost

from MongoHandler import BargainistaMongoHandler


class BargainistaPredictor():
    """
    Class to predict the market price of a flat using a previously 
    trained xgboost model.
    """

    def __init__(self, mongo_store: BargainistaMongoHandler) -> None:
        """
        BargainistaPredictor constructor. It connects to the 
        MongoDB database, retrieves the last trained model and their
        encoders to leave the model ready to perform predictions.

        Keyword arguments:
        mongo_store                 --  MongoDB instance used to store
                                        the trained model and encoders
        """
        (model, onehot_encoder,
         glmm_encoder, _) = mongo_store.retrieve_latest_model()

        self._model = pickle.loads(model)
        self._onehot_encoder = pickle.loads(onehot_encoder)
        self._glmm_encoder = pickle.loads(glmm_encoder)

    def predict(self, flats: pandas.DataFrame) -> pandas.DataFrame:
        """
        Predict the market prices of the provided flats.

        Keyword arguments:
        flats   -- Pandas dataframe with the flats whose price has to be
                   predicted

        Returns:
        prices  -- Pandas dataframe with the predicted market prices
        """
        flats_attributes = flats.drop(["id", "price"], axis=1)

        flats_attributes = self._onehot_encoder.transform(flats_attributes)
        flats_attributes = self._glmm_encoder.transform(flats_attributes)

        return self._model.predict(flats_attributes)
