import base64
import json
import requests
import time
from typing import Tuple, List


class BargainistaExtractor():
    """ 
    Class to load the idealista credentials and perform queries
    against its API.
    """

    def __init__(self, key: str, secret: str,
                 location: Tuple[float, float, float]) -> None:
        """
        BargainistaExtractor constructor. It retrieves an OAuth2 token
        from the idealista API, allowing to retrieve data from the site.

        Keyword arguments:
        key         -- API key
        secret      -- API secret
        location    -- Latitude, longitude (WGS84) and radius (m) 
        """
        self.location = location
        self.token = self._get_oauth_token(key, secret)

    def _get_oauth_token(self, key: str, secret: str) -> str:
        """
        Leverages the API key and secret to obtain an OAuth2 token.

        Keyword arguments:
        key         -- API key
        secret      -- API secret

        Returns:
        token   -- OAuth2 access token
        """
        message = f"{key}:{secret}"
        message_b64 = base64.b64encode(message.encode()).decode()

        idealista_oauth_url = "https://api.idealista.com/oauth/token"
        headers = {
            "Authorization": f"Basic {message_b64}",
            "Content-Type": "application/x-www-form-urlencoded;" +
            "charset=UTF-8"
        }
        parameters = {
            "grant_type": "client_credentials",
            "scope": "read"
        }

        token_request: requests.Response = requests.post(
            url=idealista_oauth_url,
            headers=headers,
            params=parameters
        )

        return json.loads(token_request.text)["access_token"]

    def get_flats(self, page_number: int) -> Tuple[List, int]:
        """
        Performs a query against the idealista API to obtain a list of
        up to 50 flats. 

        Keyword arguments:
        page_number -- Number of the page to be retrieved

        Returns:
        results         -- List of dictionaries holding the properties
        number of pages -- Total number of pages in the query results
        """
        idealista_api_url = "https://api.idealista.com/3.5/es/search"
        headers = {
            "Authorization": "Bearer " + self.token,
            "Content-Type": "Content-Type: multipart/form-data;"
        }
        parameters = {
            "operation": "sale",
            "center": f"{self.location[0]},{self.location[1]}",
            "distance": f"{self.location[2]}",
            "propertyType": "homes",
            "maxItems": "50",
            "locale": "es",
            "numPage": str(page_number),
            "sinceDate": "W",
            "order": "publicationDate",
            "sort": "desc",
            "hasMultimedia": "1",
            "preservation": "good"
        }

        try:
            query_response: requests.Response = requests.post(
                url=idealista_api_url,
                headers=headers,
                params=parameters
            )

            if (query_response.ok):
                query_results = query_response.json()
                time.sleep(1)  # 1 req/s maximum
                return (query_results["elementList"],
                        query_results["totalPages"])
            else:
                raise ConnectionError("API call returned >400 code.")

        except Exception:
            print(f"API call to retrive page {page_number} wasn't answered.")
            return (dict(), -1)
