import pymongo
import time
from typing import List, Tuple


class BargainistaMongoHandler():
    """
    Class to load and retrieve the idealista raw data from the MongoDB
    store.
    """

    def __init__(self, host: str, port: int, username: str,
                 password: str, auth_db: str, city: str) -> None:
        """
        BargainistaMongoHandler constructor. It connects to the MongoDB
        database, allowing to insert and retrieve data from it.

        Keyword arguments:
        host        -- MongoDB server hostname
        port        -- MongoDB server port
        username    -- MongoDB user
        password    -- MongoDB user's password
        auth_db     -- MongoDB user's database
        city        -- City where flats are being studied
        """
        try:
            self._mongo_client = pymongo.MongoClient(
                host=host,
                port=port,
                username=username,
                password=password,
                authSource=auth_db,
                connect=True
            )
            self._mongo_client.server_info()
        except pymongo.errors.ServerSelectionTimeoutError:
            raise Exception("Couldn't access the MongoDB instance. " +
                            "Check connectivity.")
        self._store = self._mongo_client.bargainista[city]
        self._model_store = self._mongo_client.bargainista[city+"_model"]

    def __del__(self) -> None:
        """
        BargainistaMongoHandler destructor. It gracefully closes the
        connections to the MongoDB instance.
        """
        self._mongo_client.close()

    def _exists_flat(self, id: int) -> bool:
        """
        Checks whether a flat is in the database already.

        Keyword arguments:
        id      -- propertyCode of the flat

        Returns:
        success -- True if the flat exists, False otherwise
        """

        if self._store.count_documents({"propertyCode": id}) > 0:
            return True
        else:
            return False

    def insert_flat(self, flat: dict) -> bool:
        """
        Inserts a flat into the MongoDB.

        Keyword arguments:
        flat    -- Dictionary with the information about one flat

        Returns:
        success -- True if the insertion succeded, False if it didn't
                   succeed or if the property was already in the DB
        """
        id = flat["propertyCode"]
        if (self._exists_flat(id)):
            return False
        else:
            return self._store.insert_one(flat).acknowledged

    def retrieve_flat(self, id: int) -> dict():
        """
        Retrieves all the information regarding a flat.

        Keyword arguments:
        id      -- propertyCode of the flat

        Returns:
        flat    -- Dictionary with the information about one flat
        """
        return self._store.find_one({"propertyCode": id})

    def retrieve_latest(self, n: int) -> List[int]:
        """
        Retrieve the "n" latest flats inserted in the store.

        Keyword arguments:
        n       -- Number of flats to retrieve

        Returns:
        flats   -- List of propertyCode id's of the latest flats
        """
        flat_cursor = self._store.find(sort=[("timestamp", -1)], limit=n)
        flats = []
        for flat in flat_cursor:
            flats.append(flat["propertyCode"])
        return flats

    def retrieve_new_flats(self, timestamp: float) -> pymongo.cursor.Cursor:
        """
        Retrieves all the flats with timestamps higher than the 
        specified.

        Keyword arguments:
        time    -- timestamp of the latest flat added to the postgres
                   database

        Returns:
        flats   -- Cursor with the dictionaries holding the flats 
                   information
        """
        return self._store.find({"timestamp": {"$gt": timestamp}})

    def save_model(self, model: bytes, onehot_encoder: bytes,
                   glmm_encoder: bytes) -> bool:
        """
        Saves the xgboost model with its related encoders into the 
        MongoDB.

        Keyword arguments:
        model           -- pickled XGBRegressor object
        onehot_encoder  -- pickled OneHotEncoder object
        glmm_encoder    -- pickled GLMMEncoder object

        Returns:
        success -- True if the insertion succeded, False if it didn't
        """
        model_data = {"model": model, "onehot_encoder": onehot_encoder,
                      "glmm_encoder": glmm_encoder, "timestamp": time.time()}
        return self._model_store.insert_one(model_data)

    def retrieve_latest_model(self) -> Tuple[bytes, bytes, bytes, float]:
        """
        Retrieves the latest xgboost model with its related encoders 
        from the MongoDB.

        Returns:
        model           -- pickled XGBRegressor object
        onehot_encoder  -- pickled OneHotEncoder object
        glmm_encoder    -- pickled GLMMEncoder object
        timestamp       -- unix timestamp of model training
        """
        model_data = self._model_store.find_one(sort=[("timestamp", -1)])

        return (model_data["model"], model_data["onehot_encoder"],
                model_data["glmm_encoder"], model_data["timestamp"])
