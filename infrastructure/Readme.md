# Bargainista infrastructure
To support the work of the GitLab CI/CD runners we'll need the following elements:
* A MongoDB instance
* A Postgres instance
* A gunicorn server exposed to the internet

These can be provisioned in a variety of providers, from managed public cloud solutions to self-hosted in bare metal.

In here I share the details of my particular implementation. Where all the three live in a cheap VPS instance.

## Databases
Both databases are deployed using their official docker images via docker-compose. To increase their security we limit their connectivity to the outside.

A dedicated user that can only create TCP tunnels through SSH is created. A tunnel is created between the runner and the database hosting so that a connection can be established. Once the connection is over, the tunnel is killed, keeping both databases safe.

Before continuing you'll need to set the `BARGAINISTA` env variable to the path of your bargainista directory, or run the `set_environment.sh` script to add it to `/opt/env.sh`.

### bargainister ssh-tunnel user
To create the user symply run the script `bargainister/create_user.sh`. 

The script will create a user with its home in the `bargainister/home/` directory. Inside, in `.ssh/` you'll find the SSH key pair. You'll need the private part of the key to be able to open the SSH tunnel.

### MongoDB instance
To deploy the mongo instance, create the `mongodb/-compose.yml` and `mongo-init.js` from the templates and run the `mongodb/deploy.sh` script. It will deploy the docker image via docker-compose and set the log directoy.

### Postgres instance
To deploy the postgres instance, create the `postgres/docker-compose.yml` from the template and run the `postgres/deploy.sh` script. It will deploy the docker image via docker-compose and set the log directoy.

### Back-up strategy
Both databases live in the same machine, making it really vulnerable to data loss in the event of a failure of the VPS.

To prevent it, you can set a crontab job to perform a backup of the docker volumes overnight. To do so, run the `set_crontab.sh` scripts in the `mongodb/` and `postgres/` directories. The script will schedule running the `backup_and_upgrade.sh` scripts. And these will create backups in the `/backups` directory. Copy these off-site to be able to restore (via the `restore.sh` scripts) in the event of catastrophe.

For these to properly run you'll need to define these bash functions somewhere in your `.bashrc`:

```bash
function backup-volume () {
    docker run --rm -v "$2:/$2" -v "/backup/$1:/backup" debian tar -czf /backup/$2.tar.gz $2
}

function restore-volume () {
    docker run --rm -v "$2:/$2" -v "/backup/$1:/backup" debian tar -xf /backup/$2.tar.gz -C /$2 --strip 1
}
```
## Dashboard
Finally, to serve the Bargainista frontend we'll need a server with gunicorn and a reverse proxy.

In our case, install gunicorn and the rest of the dependencies, and an Apache httpd instance with:

```bash
conda env create -f /environments/visualizer.yml
apt --install-recommends --install-suggests install apache2 libapache2-mod-proxy-* certbot python-certbot-apache 
a2enmod proxy* rewrite deflate headers ssl cgi expires
echo 'SSLProtocol all -SSLv2 -SSLv3' >> /etc/apache2/apache2.conf
echo 'SSLHonorCipherOrder on' >> /etc/apache2/apache2.conf
echo 'SSLCipherSuite "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS +RC4 RC4"' >> /etc/apache2/apache2.conf
service apache2 reload

```

Configure accordingly the `bargainista-dashboard.service` and `bargainista.conf` templates and execute the `deploy.sh` script. The script will set up the httpd reverse proxy with a LetsEncrypt SSL certificate, and it will also set upt a systemd service to run the gunicorn server.