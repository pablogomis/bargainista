#! /bin/bash

. /opt/env.sh
. $CONFIG_DIRECTORY/.bash_functions
export INSTALLER=$BARGAINISTA/infrastructure/postgres
export PATH=$PATH:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin

docker-compose -f docker-compose.yml -p bargainista_postgres up -d
mkdir -p /var/log/postgres/
chmod 777 /var/log/postgres/
