#! /bin/bash

. /opt/env.sh
. $CONFIG_DIRECTORY/.bash_functions
export INSTALLER=$BARGAINISTA/infrastructure/postgres
export PATH=$PATH:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin

docker-compose -f $INSTALLER/docker-compose.yml -p bargainista_postgres stop
backup-volume bargainista_postgres bargainista_postgres_db
docker-compose -f $INSTALLER/docker-compose.yml -p bargainista_postgres rm -f
docker-compose -f $INSTALLER/docker-compose.yml -p bargainista_postgres pull
docker-compose -f $INSTALLER/docker-compose.yml -p bargainista_postgres up -d

