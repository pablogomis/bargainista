CREATE TABLE types (
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    type TEXT NOT NULL
);
CREATE TABLE cities(
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    city TEXT NOT NULL
);
CREATE TABLE districts(
    id INT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    district TEXT NOT NULL,
    city SMALLINT NOT NULL,
    CONSTRAINT fk_city FOREIGN KEY(city) REFERENCES cities(id)
);
CREATE TABLE estate (
    id INT PRIMARY KEY,
    price MONEY NOT NULL,
    market_price MONEY DEFAULT NULL,
    size REAL NOT NULL,
    rooms SMALLINT NOT NULL,
    bathrooms SMALLINT NOT NULL,
    floor SMALLINT NOT NULL,
    elevator BOOLEAN NOT NULL,
    type SMALLINT NOT NULL,
    parking BOOLEAN NOT NULL,
    exterior BOOLEAN NOT NULL,
    status BOOLEAN NOT NULL,
    development BOOLEAN NOT NULL,
    district SMALLINT NOT NULL,
    latitude REAL NOT NULL,
    longitude REAL NOT NULL,
    timestamp TIMESTAMP NOT NULL,
    CONSTRAINT fk_type FOREIGN KEY(type) REFERENCES types(id),
    CONSTRAINT fk_district FOREIGN KEY(district) REFERENCES districts(id)
);