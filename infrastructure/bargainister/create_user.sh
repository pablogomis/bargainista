# Create the bargainister user
addgroup bargainister
adduser --system --shell /bin/bash --disabled-password --home $PWD/home bargainister --ingroup bargainister

# Create the authorized_keys
sudo -u bargainister ssh-keygen -f $PWD/home/.ssh/id_rsa -N "" -q
sudo -u bargainister cat $PWD/home/.ssh/id_rsa.pub >> $PWD/home/.ssh/authorized_keys

# Remove the ability to log in
usermod --shell /bin/false bargainister

# Restrict the bargainister user to "tunnel-only" user
echo -e '\nMatch User bargainister
\tForceCommand echo "This user is only valid to open TCP tunnels" 
\tAllowAgentForwarding no
\tAllowTcpForwarding no
\tX11Forwarding no
\tGatewayPorts no
\tPermitTunnel no
\tPermitOpen localhost:28018
\tPermitOpen localhost:5432' >> /etc/ssh/sshd_config
