cp bargainista.conf /etc/apache2/sites-available
chown root:root /etc/apache2/sites-available/bargainista.conf
mkdir -p /var/log/apache2/bargainista.gomis.xyz
service apache2 stop
certbot certonly -d bargainista.gomis.xyz --standalone
a2ensite bargainista.conf
service apache2 start

cp bargainista-dashboard.service /etc/systemd/system
chown root:root /etc/systemd/system/bargainista-dashboard.service
chmod 644 /etc/systemd/system/bargainista-dashboard.service
systemctl daemon-reload
systemctl enable bargainista-dashboard.service
systemctl start bargainista-dashboard.service
