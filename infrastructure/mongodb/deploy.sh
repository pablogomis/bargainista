#! /bin/bash

. /opt/env.sh
. $CONFIG_DIRECTORY/.bash_functions
export INSTALLER=$BARGAINISTA/infrastructure/mongodb
export PATH=$PATH:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin

docker-compose -f docker-compose.yml -p bargainista_mongodb up -d
mkdir -p /var/log/mongodb/
touch /var/log/mongodb/bargainista.log
chown mongodb:mongodb /var/log/mongodb/bargainista.log
