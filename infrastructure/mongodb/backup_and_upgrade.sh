#! /bin/bash

. /opt/env.sh
. $CONFIG_DIRECTORY/.bash_functions
export INSTALLER=$BARGAINISTA/infrastructure/mongodb
export PATH=$PATH:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/bin

docker-compose -f $INSTALLER/docker-compose.yml -p bargainista_mongodb stop
backup-volume bargainista_mongodb bargainista_mongodb_db
docker-compose -f $INSTALLER/docker-compose.yml -p bargainista_mongodb rm -f
docker-compose -f $INSTALLER/docker-compose.yml -p bargainista_mongodb pull
docker-compose -f $INSTALLER/docker-compose.yml -p bargainista_mongodb up -d

