# Bargainista
This project aims identify undervalued properties in a given city, listing the best bargains in Idealista in a web dashboard.

## Technical implementation
The project is built in four steps consisting on five asynchronous processes:

1. An ETL pipeline that is divided in two steps.
    * The `extract` process queries the Idealista API and stores the last ads in a MongoDB instance.
    * The `load` process cleans and structures (getting rid of outliers and duplicated entries) the raw data before inserting it into a Postgres database.
2. The `train` step where the clean data is fed to an XGBoost regressor that learns how to predict the properties market price. Once the training is complete, the trained model is pickled and stored in the MongoDB instance.
3. A `predict` step, where the latest trained model is retrieved and it is used to predict the market prices for the new property listings. These listings are updated in the Postgres database with their market price.
4. Last, an interactive dashboard built with Plotly Dash (Flask) retrieves the discounted properties from the Postgres instance and it serves the data in this web application.

The first four steps are implemented as Python scripts in the `src/` directory, while the dashboard application server with its CSS styles can be found in the `dashboard/` directory.

Most of the logic is implemented in self-contained libraries in the `modules/` directory. These provide classes and functions to handle the Mongo and Postgres instances, the Idealista API, the XGBoost model training and inference, and the data wrangling.

## Getting started
To deploy the Bargainista project you'll first need to prepare a MongoDB and a Postgres instance, as well as a gunicorn server with a reverse proxy to serve the dashboard.

### Infrastructure
The project is meant to be ran in GitLab runners scheduling jobs for all the steps periodically. Its heart lies in its `gitlab-ci.yml` GitLab CI/CD pipeline. It defines the `extract`, `load`, `train` and `predict` jobs that retrieve and work the data and trained models into the MongoDB and Postgres instances. Whereas the dashboard is served through gunicorn with an Apache httpd server as a reverse proxy.

The `infrastructure/` directory holds all the relevant information to deploy and secure the Postgres, MongoDB and dashboard instances in a debian-based machine.
### Setting the credentials
The details of these instances should be dumped into the env variables defined in the `dump_vars.sh` script, which is used to fill the `credentials.py` file used to feed the information to the Bargainsita. Alternatively, you can create it from the `credentials.py.template` file manually.

#### Establishing the SSH tunnels
In the event that you chose to secure your MongoDB and Postgres instances by limiting their traffic to localhost, you'll need to open an SSH tunnel to be able to reach them. If you set the key as an env variable, the `dump_vars.sh` should have created the `bargainister.key` file. To create the tunnels type in your terminal:

```bash
$ ssh ${SSH_USER}@${MONGO_SERVER} -N -L ${MONGO_PORT}:${MONGO_HOST}:${MONGO_PORT} -i bargainister.key -o StrictHostKeyChecking=no &
$ ssh ${SSH_USER}@${POSTGRES_SERVER} -N -L ${POSTGRES_PORT}:${POSTGRES_HOST}:${POSTGRES_PORT} -i bargainister.key -o StrictHostKeyChecking=no &
```

### Installing the dependencies
Once the credentials are set, the next step is to install the project dependencies. First we-ll need a fully working installation of the [Anaconda distribution](https://www.anaconda.com/products/individual#Downloads).

To set a development environment just run `set_environment.sh`. The script will set the conda environment `bargainista` defined in `environment.yml` with all the dependencies needed to run the whole project.

Shorter versions of `environment.yml` can be found in the `environments` directory, providing the dependencies needed to run individual steps.

### Testing the code 
Now we are ready to run the project, but before we'll want to make sure that everything is working as expected. To so so simply run the `pytest` suite by executing:

``` bash
$ pytest
```

### Running the pipeline
Although the project is meant to run in GitLab, it can be executed manually by running:

``` bash
$ python src/extract.py --city ${CITY} --max_pages ${MAX_API_REQUESTS}
$ python src/load.py --city ${CITY}
$ python src/train.py --city ${CITY} --output_path output --workers 1 --threshold 0.8
$ python src/predict.py --city ${CITY}
```

After the `train` step ran, it should create the `output/` directory where some debugging graphs are stored in order to check the model performance.

### Using the site
The last step is about making the dashboard accessible. A systemd template to create a bargainista-dashboard service is available in the `infrastructure/` directory. In the case that you want to run the dashboard manually, just run it from the `dashboard/` directory:

```bash
$ gunicorn -w 4 dashboard:server
```

Finally, navigate to the dashboard and play around with the application. The data showed in the dashboard is retrieved from the Postgres instance every single time a user enters into the web, always showing the freshest data processed by the pipeline.