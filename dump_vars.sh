echo -e "key = \"${IDEALISTA_KEY}\"
secret = \"${IDEALISTA_SECRET}\"
mongo_user = \"${MONGO_USER}\"
mongo_pass = \"${MONGO_PASS}\"
mongo_auth_db = \"${MONGO_AUTH_DB}\"
mongo_host = \"${MONGO_HOST}\"
mongo_port = ${MONGO_PORT}
postgres_user = \"${POSTGRES_USER}\"
postgres_pass = \"${POSTGRES_PASS}\"
postgres_db = \"${POSTGRES_DB}\"
postgres_host = \"${POSTGRES_HOST}\"
postgres_port = ${POSTGRES_PORT}
mapbox_access_token = \"${MAPBOX_TOKEN}\"" > credentials.py
echo "${BARGAINISTER_SSH_KEY}" > bargainister.key
chmod 600 bargainister.key